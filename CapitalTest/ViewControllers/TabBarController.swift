//
//  TabBarController.swift
//  CapitalTest
//
//  Created by Serg on 16.06.2022.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        tabBar.backgroundColor = .systemBrown
        
        setupVCs()
    }

    private func createNavController(
        for rootViewController: UIViewController,
        title: String,
        image: UIImage,
        navbarColor: UIColor
    ) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        
        rootViewController.navigationItem.title = title
        rootViewController.navigationController?.navigationBar.backgroundColor = navbarColor
        
        return navController
    }
    
    private func setupVCs() {
        viewControllers = [
            createNavController(
                for: MeetingEditViewController(),
                   title: NSLocalizedString("Создать встречу", comment: ""),
                   image: UIImage(systemName: "gear")!,
                   navbarColor: .systemMint),
            createNavController(
                for: MeetingViewController(),
                   title: NSLocalizedString("Встреча", comment: ""),
                   image: UIImage(systemName: "person.circle")!,
                   navbarColor: .systemTeal)
        ]
    }
}
