//
//  MeetingViewController.swift
//  CapitalTest
//
//  Created by Serg on 16.06.2022.
//

import UIKit
import SnapKit
import Combine
import RealmSwift

class MeetingViewController: UIViewController {

    private var token: NotificationToken?
    private var cancellables: [AnyCancellable] = []
    
    private let activityView: UIView = {
        $0.backgroundColor = .clear
        $0.isHidden = true
        $0.addActivityIndicator()
        return $0
    }(UIView())
    
    private let vStackView: UIStackView = {
        $0.axis = .vertical
        $0.alignment = .center
        $0.distribution = .fill
        $0.spacing = 16.0
        return $0
    }(UIStackView())

    // MARK: - Class funcs
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        setupView()
        sink()
    }
}

// MARK: - View setup

private extension MeetingViewController {
    func setupView() {
        initialPopulate()
        view.addSubview(vStackView)
        view.addSubview(activityView)
        
        vStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset((navigationController?.navigationBar.bounds.height ?? 80) + 20)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
        
        activityView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: - Private funcs

private extension MeetingViewController {
    
    func initialPopulate() {
        guard let meeting = RealmManager.shared().getMeeting() else {
            return
        }
        
        let labels = createLabels(from: meeting)
        
        populateData(with: labels)
    }
    
    func populateData(with labels: [UILabel]) {
        vStackView.arrangedSubviews.forEach { view in
            view.removeFromSuperview()
        }

        labels.forEach { label in
            vStackView.addArrangedSubview(label)
        }
    }
    
    func getLabel(with text: String) -> UILabel {
        let label: UILabel = {
            $0.text = text
            $0.numberOfLines = 0
            $0.lineBreakMode = .byWordWrapping

            $0.setContentHuggingPriority(.required, for: .vertical)
            $0.setContentCompressionResistancePriority(.required, for: .horizontal)
            $0.setContentCompressionResistancePriority(.required, for: .vertical)
            return $0
        }(UILabel())
        
        return label
    }
    
    func createLabels(from meeting: MeetingObject) -> [UILabel] {

        var labels: [UILabel] = []
        
        for type in MeetingObject.Naming.allCases {
            switch type {
                
            case .name:
                labels.append(getLabel(with: meeting.name))
            case .info:
                labels.append(getLabel(with: meeting.info))
            case .dateStart:
                labels.append(getLabel(with: meeting.dateStart))
            case .dateEnd:
                labels.append(getLabel(with: meeting.dateEnd))
            case .place:
                labels.append(getLabel(with: meeting.place))
            }
        }
        
        return labels
    }

    func sink() {
        token = RealmManager.shared().startObserving(completion: { [weak self] obj in
            if let labels = self?.createLabels(from: obj) {
                self?.populateData(with: labels)
                self?.activityView.isHidden = true
            }
        })
        
        RealmManager.shared().busyPublisher.sink { [weak self] _ in
            DispatchQueue.main.async {
                self?.activityView.isHidden = false
            }
        }.store(in: &cancellables)
    }
}

