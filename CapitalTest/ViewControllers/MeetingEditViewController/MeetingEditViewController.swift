//
//  MeetingEditViewController.swift
//  CapitalTest
//
//  Created by Serg on 16.06.2022.
//

import UIKit
import SnapKit
import RealmSwift
import Combine

class MeetingEditViewController: UIViewController {
    
    private var token: NotificationToken?
    private var cancellables: [AnyCancellable] = []
    
    private enum DataState {
        case loading
        case loaded(with: MeetingObject)
    }
    
    private var dataState: DataState = .loading {
        didSet {
            //update meeting
            if case .loaded(with: let meeting) = dataState {
                RealmManager.shared().update(for: meeting)
            }
        }
    }
    
    // MARK: - UI properties
    
    private let activityView: UIView = {
        $0.backgroundColor = .clear
        $0.isHidden = true
        $0.addActivityIndicator()
        return $0
    }(UIView())
    
    private let nameTextField: UITextField = {
        $0.layer.cornerRadius       = 12
        $0.placeholder              = "Имя"
        $0.keyboardType             = .default
        return $0
     }(UITextField())
    
    private let infoTextField: UITextField = {
        $0.layer.cornerRadius       = 12
        $0.placeholder              = "Инфо"
        $0.keyboardType             = .default
        return $0
     }(UITextField())
    
    private let dateStartTextField: UITextField = {
        $0.layer.cornerRadius       = 12
        $0.placeholder              = "Время начала"
        $0.keyboardType             = .default
        return $0
     }(UITextField())
    
    private let dateEndTextField: UITextField = {
        $0.layer.cornerRadius       = 12
        $0.placeholder              = "Время окончания"
        $0.keyboardType             = .default
        return $0
     }(UITextField())
    
    private let placeTextField: UITextField = {
        $0.layer.cornerRadius       = 12
        $0.placeholder              = "Место"
        $0.keyboardType             = .default
        return $0
     }(UITextField())
    
    
    private let button: UIButton = {
        $0.setTitle("Update", for: .normal)
        return $0
    }(UIButton(type: .system))
    
    private let vStackView: UIStackView = {
        $0.axis = .vertical
        $0.alignment = .center
        $0.distribution = .fill
        $0.spacing = 16.0
        return $0
    }(UIStackView())

    // MARK: - Class funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        setupView()
        setupAction()
        initialPopulate()
        sink()
    }
}

// MARK: - UI setup

private extension MeetingEditViewController {
    func setupView() {
        vStackView.addArrangedSubview(nameTextField)
        vStackView.addArrangedSubview(infoTextField)
        vStackView.addArrangedSubview(dateStartTextField)
        vStackView.addArrangedSubview(dateEndTextField)
        vStackView.addArrangedSubview(placeTextField)
        
        view.addSubview(vStackView)
        view.addSubview(button)
        view.addSubview(activityView)

        vStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset((navigationController?.navigationBar.bounds.height ?? 80) + 20)
            $0.leading.trailing.equalToSuperview()
        }
        
        button.snp.makeConstraints {
            $0.top.equalTo(vStackView.snp.bottom).offset(16)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
        
        activityView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func setupAction() {
        button.addTarget(self, action: #selector(buttonActionHandler(_:)), for: .touchUpInside)
    }
    
    @objc func buttonActionHandler(_ button: UIButton) {
        createMeeting()
    }
}


// MARK: - Private funcs

private extension MeetingEditViewController {
    func initialPopulate() {
        guard let meeting = RealmManager.shared().getMeeting() else {
            return
        }
        
        populateData(with: meeting)
    }
    
    func populateData(with meeting: MeetingObject) {
        
        for type in MeetingObject.Naming.allCases {
            switch type {
                
            case .name:
                nameTextField.text = meeting.name
            case .info:
                infoTextField.text = meeting.info
            case .dateStart:
                dateStartTextField.text = meeting.dateStart
            case .dateEnd:
                dateEndTextField.text = meeting.dateEnd
            case .place:
                placeTextField.text = meeting.place
            }
        }

    }

    func createMeeting() {
        let meeting = MeetingObject()
        meeting.name = nameTextField.text ?? ""
        meeting.info = infoTextField.text ?? ""
        meeting.dateStart = dateStartTextField.text ?? ""
        meeting.dateEnd = dateEndTextField.text ?? ""
        meeting.place = placeTextField.text ?? ""
        
        dataState = .loaded(with: meeting)
    }
    
    func sink() {
        token = RealmManager.shared().startObserving(completion: { [weak self] obj in
            self?.populateData(with: obj)
            self?.activityView.isHidden = true
        })
        
        RealmManager.shared().busyPublisher.sink { [weak self] _ in
            self?.activityView.isHidden = false
        }.store(in: &cancellables)
    }
}


