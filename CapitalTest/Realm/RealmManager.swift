//
//  RealmManager.swift
//  CapitalTest
//
//  Created by Serg on 16.06.2022.
//

import RealmSwift
import Combine

fileprivate protocol MeetingProtocol: AnyObject {
    associatedtype Meeting: Object

    var busyPublisher: PassthroughSubject<Void, Never> { get }
    func getMeeting() -> Meeting?
}

public class RealmManager: MeetingProtocol {
    typealias Meeting = MeetingObject
    
    private let realm = try! Realm()
    private var meeting: MeetingObject?
    
    public let busyPublisher = PassthroughSubject<Void, Never>()
    private static var sharedRealmManager: RealmManager = RealmManager()

    // MARK: - Class funcs
    private init() {
        meeting = startSession()
    }

    public class func shared() -> RealmManager {
        return sharedRealmManager
    }
}

// MARK: - private funcs
private extension RealmManager {
    private func startSession() -> Meeting {
        guard let meeting = getMeeting() else {
            let meeting = MeetingObject()
            
            try! realm.write {
                realm.add(meeting)
            }
            return meeting
        }
        
        return meeting
    }
}

// MARK: - Public accessors
public extension RealmManager {
    func getMeeting() -> MeetingObject? {
         let meetings: Results<MeetingObject> = realm.objects(MeetingObject.self)
         return meetings.first
    }
    
    func update(for temp: MeetingObject) {
        busyPublisher.send(())
        
        //по заданию надо подождать 3 сек
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            guard let meeting = self.getMeeting() else {
                assertionFailure("Cannot be empty")
                return
            }
            
            do {
                try self.realm.write {
                    meeting.name = temp.name
                    meeting.info = temp.info
                    meeting.dateStart = temp.dateStart
                    meeting.dateEnd = temp.dateEnd
                    meeting.place = temp.place
                }
            } catch (let error) {
                print(error)
            }
        }

    }
    
    func startObserving(completion: @escaping (_ obj: MeetingObject)->Void) -> NotificationToken? {
        return meeting?.observe { change in
            switch change {

            case .error(_):
                ()
                //если нужен вывод ошибок то лучше использовать паблишер
                //self.updatePublisher.send(completion: .failure(err))
            case .change(_, let two):
                let coordObj = MeetingObject()
                two.forEach { propChange in
                    let name = MeetingObject.Naming(rawValue: propChange.name)
                    if let val = propChange.newValue as? String {
                        switch name {
                        case .name:
                            coordObj.name = val
                        case .info:
                            coordObj.info = val
                        case .dateStart:
                            coordObj.dateStart = val
                        case .dateEnd:
                            coordObj.dateEnd = val
                        case .place:
                            coordObj.place = val
                        case .none:
                            ()
                        }
                    }
                }
                completion(coordObj)
                
            case .deleted:
                //про удаление в задаче не говорилось
                ()
            }
        }
    }
}
