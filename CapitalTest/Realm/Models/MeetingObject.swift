//
//  MeetingObject.swift
//  CapitalTest
//
//  Created by Serg on 16.06.2022.
//

import RealmSwift

public class MeetingObject: Object {
    @Persisted var name: String
    @Persisted var info: String
    @Persisted var dateStart: String
    @Persisted var dateEnd: String
    @Persisted var place: String
    
    public enum Naming: String, CaseIterable {
        case name
        case info
        case dateStart
        case dateEnd
        case place
    }
}
